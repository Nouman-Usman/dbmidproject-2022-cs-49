﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbMidProjCS49.UI;

namespace DbMidProjCS49
{
    public static class ValidationsAndParsings
    {
        public static bool isNumber(string s)
        {
            foreach (char c in s)
            {
                if (char.IsDigit(c))
                {
                    string pattern = @"^03\d{9}$";
                    if (Regex.IsMatch(s, pattern))
                    {
                        return true;
                    }
                    return true;
                }
            }
            return false;
        }
        public static bool IsValidEmail(string email)
        {
            string pattern = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(email);
        }

        public static bool numberVer(TextBox textBox, string fieldName)
        {
            if (!isNumber(textBox.Text))
            {
                MessageBox.Show(fieldName + " must be a number.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
        public static int parseMonthNumber(string month)
        {
            switch (month)
            {
                case "January":
                    return 1;
                case "February":
                    return 2;
                case "March":
                    return 3;
                case "April":
                    return 4;
                case "May":
                    return 5;
                case "June":
                    return 6;
                case "July":
                    return 7;
                case "August":
                    return 8;
                case "September":
                    return 9;
                case "October":
                    return 10;
                case "November":
                    return 11;
                case "December":
                    return 12;
                default:
                    return -1;
            }
        }

        public static bool isEveryFieldSet(TextBox[] textBoxes)
        {
            foreach (TextBox textBox in textBoxes)
            {
                if (string.IsNullOrWhiteSpace(textBox.Text))
                {
                    MessageBox.Show("All fields must be filled.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }

        public static string parseDate(string date)
        {
            string[] temp = date.Split(',');
            string year = temp[2];
            string[] monthAndDay = temp[1].Split(' ');
            string month = monthAndDay[1];
            string day = monthAndDay[2];
            return year + "-" + parseMonthNumber(month) + "-" + day;
        }
    }
}
