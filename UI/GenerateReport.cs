﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DbMidProjCS49.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;
using midtermProject;

namespace midtermProject
{
    public partial class GenerateReport : Form
    {
        public GenerateReport()
        {
            InitializeComponent();
        }

        

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "select GS.StudentId as [Student ID], PA.ProjectId as [Project ID], GP.GroupId as [Group ID], PA.AdvisorId as [Advisor Id], L.Value as [Advisor Role] from GroupStudent" +
                " as GS join GroupProject as GP on GP.GroupId = GS.GroupId join ProjectAdvisor as PA on PA.ProjectId = GP.ProjectId join Lookup as L on PA.AdvisorRole = L.Id";
            GeneratePDF(connection, query);
            MessageBox.Show("PDF Report has been generated");

        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "select P.FirstName, P.LastName, L.Value as [Designation], P.Email, P.Contact , A.Salary from Advisor A join Person P on A.Id = P.Id join Lookup L on A.Designation = L.Id";
            GeneratePDF2(connection, query);
            MessageBox.Show("PDF Report has been generated");
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "select S.RegistrationNo, P.FirstName, P.LastName, L.Value as [Gender], P.Contact, P.Email, P.DateOfBirth from Student S join Person P on S.Id = P.Id join Lookup L on P.Gender = L.Id";
            GeneratePDF3(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }

        private void siticoneButton4_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "  select g.Id, G.Created_On, S.RegistrationNo, GS.AssignmentDate from [Group] G join GroupStudent GS on GS.GroupId = G.Id join Student S on GS.StudentId = S.Id where GS.Status = 3";
            GeneratePDF4(connection, query);
            MessageBox.Show("PDF Report has been generated");
        }


        private void GeneratePDF(string connectionString, string query)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Document document = new Document();
                    PdfWriter.GetInstance(document, new System.IO.FileStream("ProjectReport.pdf", FileMode.Create));
                    document.Open();
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 3f, 3f, 3f, 3f, 3f });
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table.AddCell(cell);
                    }
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            table.AddCell(reader[i].ToString());
                        }
                    }
                    document.Add(table);
                    document.Close();
                    reader.Close();
                }


            }
        }

        private void GeneratePDF2(string connectionString, string query)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    Document document = new Document();
                    PdfWriter.GetInstance(document, new System.IO.FileStream("AdvisorsReport.pdf", FileMode.Create));
                    document.Open();

                    // Add a table to the PDF document
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 3f, 3f, 3f, 3f, 3f, 3f });

                    // Add headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table.AddCell(cell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            table.AddCell(reader[i].ToString());
                        }
                    }

                    // Add the table to the document
                    document.Add(table);

                    // Close the document and the reader
                    document.Close();
                    reader.Close();
                }


            }
        }
        private void GeneratePDF3(string connectionString, string query)
        {
            // Connect to the database and retrieve data
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Create a new PDF document and add a page
                    Document document = new Document();
                    PdfWriter.GetInstance(document, new System.IO.FileStream("StudentsReport.pdf", FileMode.Create));
                    document.Open();

                    // Add a table to the PDF document
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 3f, 3f, 3f, 3f, 3f, 3f, 3f });

                    // Add headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table.AddCell(cell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            table.AddCell(reader[i].ToString());
                        }
                    }

                    // Add the table to the document
                    document.Add(table);

                    // Close the document and the reader
                    document.Close();
                    reader.Close();
                }


            }
        }
        private void GeneratePDF4(string connectionString, string query)
        {
            // Connect to the database and retrieve data
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Create a new PDF document and add a page
                    Document document = new Document();
                    PdfWriter.GetInstance(document, new System.IO.FileStream("GroupsReport.pdf", FileMode.Create));
                    document.Open();

                    // Add a table to the PDF document
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 3f, 3f, 3f, 3f });

                    // Add headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table.AddCell(cell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            table.AddCell(reader[i].ToString());
                        }
                    }

                    // Add the table to the document
                    document.Add(table);

                    // Close the document and the reader
                    document.Close();
                    reader.Close();
                }


            }
        }

        private void GenerateMarksheet(string connectionString, string query)
        {
            // Connect to the database and retrieve data
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Create a new PDF document and add a page
                    Document document = new Document();
                    PdfWriter.GetInstance(document, new System.IO.FileStream("Marksheet.pdf", FileMode.Create));
                    document.Open();

                    // Add a table to the PDF document
                    PdfPTable table = new PdfPTable(reader.FieldCount);
                    table.WidthPercentage = 100;
                    table.SetWidths(new float[] { 3f, 3f, 3f, 3f, 3f, 3f, 3f });

                    // Add headers to the table
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(reader.GetName(i)));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        table.AddCell(cell);
                    }

                    // Add rows to the table
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            table.AddCell(reader[i].ToString());
                        }
                    }

                    // Add the table to the document
                    document.Add(table);

                    // Close the document and the reader
                    document.Close();
                    reader.Close();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
    }
}
