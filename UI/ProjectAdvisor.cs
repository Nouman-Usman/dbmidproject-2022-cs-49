﻿using MidProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DbMidProjCS49.UI
{
    public partial class ProjectAdvisor : Form
    {
        public ProjectAdvisor()
        {
            InitializeComponent();
        }

        private void ProjectID_Enter(object sender, EventArgs e)
        {

        }

        private void ProjectID_Click(object sender, EventArgs e)
        {
            LoadProjectID();
        }
        private void LoadProjectID()
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    ProjectID.DisplayMember = "Id";
                    ProjectID.ValueMember = "Id";
                    ProjectID.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void LoadAdvisorID()
        {
            // load advisor id into combobox
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Advisor";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    AdvisorId.DisplayMember = "Id";
                    AdvisorId.ValueMember = "Id";
                    AdvisorId.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void LoadDataIntoGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  ProjectAdvisor.AdvisorId, ProjectAdvisor.ProjectId, Project.Title as [Project Title] , ProjectAdvisor.AssignmentDate, Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole join Project on ProjectAdvisor.ProjectId = Project.Id");
            cmd.Connection = con;
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            ProjectAdvGrid.DataSource = dataTable;
            con.Close();
        }

        private void ProjectAdvisor_Load(object sender, EventArgs e)
        {
            LoadDataIntoGrid();
        }

        private void designation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ProjectID_Enter_1(object sender, EventArgs e)
        {
            LoadProjectID();
        }

        private void ProjectID_Click_1(object sender, EventArgs e)
        {
            LoadProjectID();
        }

        private void AdvisorId_Enter(object sender, EventArgs e)
        {
            LoadAdvisorID();
            LoadAdvisorRole();
        }

        private void AdvisorId_Click(object sender, EventArgs e)
        {
            LoadAdvisorID();
            LoadAdvisorRole();
        }

        private void LoadAdvisorRole()
        {
            // load advisor role into combobox against the advisor id
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Value from Lookup where Category = 'ADVISOR_ROLE'";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    AdvisorRole.DisplayMember = "Value";
                    AdvisorRole.ValueMember = "Value";
                    AdvisorRole.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            int role_advisor = 0;
            // check none of the combobox is empty and also check either it is already added in the database or not and convert the role to int and then add it to the database and then load the data into the grid
            if (ProjectID.Text != "" && AdvisorId.Text != "" && AdvisorRole.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("INSERT INTO ProjectAdvisor(AdvisorId,ProjectId,AdvisorRole,AssignmentDate) VALUES (@AdvisorId,@ProjectId, @AdvisorRole,@AssignmentDate)", con);
                cmd.Parameters.AddWithValue("@AdvisorId", AdvisorId.SelectedValue);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectID.SelectedValue);

                if (AdvisorRole.Text == "Industry Advisor")
                {
                    role_advisor = 14;
                    cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                }
                else if (AdvisorRole.Text == "Co-Advisor")
                {
                    role_advisor = 12;
                    cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                }
                else if (AdvisorRole.Text == "Main Advisor")
                {
                    role_advisor = 11;
                    cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                }


                AssignmentDate.Format = DateTimePickerFormat.Custom;
                AssignmentDate.CustomFormat = "yyyy-MM-dd";
                cmd.Parameters.AddWithValue("@AssignmentDate", AssignmentDate.Text);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Successfully saved");
                LoadDataIntoGrid();
            }
            else
            {
                MessageBox.Show("Please fill all the fields");
            }

        }

        private void ProjectAdvGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Add.Enabled = false;
            UpdateBtn.Enabled = true;
            DeleteBtn.Enabled = true;
            // when the user clicks the cell of the grid then the data of that cell will be loaded into the comboboxes and datetimepicker
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.ProjectAdvGrid.Rows[e.RowIndex];
                AdvisorId.Text = row.Cells[0].Value.ToString();
                ProjectID.Text = row.Cells[1].Value.ToString();
                AssignmentDate.Text = row.Cells[3].Value.ToString();
                AdvisorRole.Text = row.Cells[4].Value.ToString();
            }
        }

        private void ProjectAdvGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Add.Enabled = false;
            UpdateBtn.Enabled = true;
            DeleteBtn.Enabled = true;
            // when the user clicks the cell of the grid then the data of that cell will be loaded into the comboboxes and datetimepicker
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.ProjectAdvGrid.Rows[e.RowIndex];
                AdvisorId.Text = row.Cells[0].Value.ToString();
                ProjectID.Text = row.Cells[1].Value.ToString();
                AssignmentDate.Text = row.Cells[3].Value.ToString();
                AdvisorRole.Text = row.Cells[4].Value.ToString();
            }
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {            
            // update the data of the grid into the database if all the entries doesn't exist before and then load the data into the grid and also use exceptions
            if (ProjectID.Text != "" && AdvisorId.Text != "" && AdvisorRole.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId = @AdvisorId, ProjectId = @ProjectId, AdvisorRole = @AdvisorRole, AssignmentDate = @AssignmentDate WHERE AdvisorId = @AdvisorId AND ProjectId = @ProjectId", con);
                cmd.Parameters.AddWithValue("@AdvisorId", AdvisorId.SelectedValue);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectID.SelectedValue);

                if (AdvisorRole.Text == "Industry Advisor")
                {
                    cmd.Parameters.AddWithValue("@AdvisorRole", 14);

                }
                else if (AdvisorRole.Text == "Co-Advisor")
                {
                    cmd.Parameters.AddWithValue("@AdvisorRole", 12);

                }
                else if (AdvisorRole.Text == "Main Advisor")
                {
                    cmd.Parameters.AddWithValue("@AdvisorRole", 11);

                }
                AssignmentDate.Format = DateTimePickerFormat.Custom;
                AssignmentDate.CustomFormat = "yyyy-MM-dd";
                cmd.Parameters.AddWithValue("@AssignmentDate", AssignmentDate.Text);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Successfully updated");
                LoadDataIntoGrid();
            }
            else
            {
                MessageBox.Show("Please fill all the fields");
            }

        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            // delete the data of the grid from the database and then load the data into the grid and also use exceptions
            if (ProjectID.Text != "" && AdvisorId.Text != "" && AdvisorRole.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId AND ProjectId = @ProjectId", con);
                cmd.Parameters.AddWithValue("@AdvisorId", AdvisorId.SelectedValue);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectID.SelectedValue);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Successfully deleted");
                LoadDataIntoGrid();
            }
            else
            {
                MessageBox.Show("Please fill all the fields");
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
    }
}
