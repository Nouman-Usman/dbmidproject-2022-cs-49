﻿using MidProject;
using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DbMidProjCS49.UI
{
    public partial class ProjectMenu : Form
    {
        public ProjectMenu()
        {
            InitializeComponent();
        }

        private void richTextBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (description.Text == "Write description of the project here")
            {
                description.Text = "";
            }
        }

        private void description_Layout(object sender, LayoutEventArgs e)
        {

        }

        private void description_Click(object sender, EventArgs e)
        {
            if (description.Text == "Write description of the project here")
            {
                description.Text = "";
            }
        }

        private void title_MouseClick(object sender, MouseEventArgs e)
        {
            if (title.Text == "Write title of the Project here")
            {
                title.Text = "";
            }
        }

        private void title_Leave(object sender, EventArgs e)
        {
            if (title.Text == "")
            {
                title.Text = "Write title of the Project here";
            }
        }

        private void title_Enter(object sender, EventArgs e)
        {
            if (title.Text == "Write title of the Project here")
            {
                title.Text = "";
            }
        }

        private void description_Leave(object sender, EventArgs e)
        {
            if (description.Text == "")
            {
                description.Text = "Write description of the project here";
            }
        }

        private void ProjectMenu_Load(object sender, EventArgs e)
        {
            Add.Enabled = true;
            UpdateBtn.Enabled = false;
            DeleteBtn.Enabled = false;
            LoadDataIntoGrid();
        }
        private void LoadDataIntoGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Description, Title \r\nfrom Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            ProjectGrid.DataSource = dataTable;
        }
        private bool IsProjectTitleUnique(string projectTitle, SqlConnection connection)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Project WHERE Title = @Title", connection);
                cmd.Parameters.AddWithValue("@Title", projectTitle);
                int count = (int)cmd.ExecuteScalar();
                return count == 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error checking project title uniqueness: " + ex.Message);
                return false;
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                // Check if the project with the same title already exists

                if (!IsProjectTitleUnique(title.Text, con))
                {
                    MessageBox.Show("A project with this title already exists.");
                    return;
                }
                else if (title.Text == "Write title of the Project here" || description.Text == "Write description of the project here")
                {
                    MessageBox.Show("Please fill all fields");
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("INSERT INTO Project (Description, Title) VALUES (@Description, @Title)", con);
                    cmd.Parameters.AddWithValue("@Description", description.Text);
                    cmd.Parameters.AddWithValue("@Title", title.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    LoadDataIntoGrid();
                }

            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            // Delete from database and refresh the grid
            try
            {
                if (ProjectGrid.SelectedRows.Count > 0)
                {
                    DataGridViewRow selectedRow = ProjectGrid.SelectedRows[0];
                    int projectId = (int)selectedRow.Cells["Id"].Value;
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[ProjectAdvisor] where ProjectId = @ProjectId", con1);
                    cmd1.Parameters.AddWithValue("@ProjectId", projectId);
                    cmd1.ExecuteNonQuery();

                    var con3 = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete from GroupProject where ProjectId = @ProjectId", con3);
                    cmd.Parameters.AddWithValue("@ProjectId", projectId);
                    cmd.ExecuteNonQuery();

                    var con4 = Configuration.getInstance().getConnection();
                    SqlCommand cmd2 = new SqlCommand("Delete from Project where Id = @Id", con4);
                    cmd2.Parameters.AddWithValue("@Id", projectId);
                    cmd2.ExecuteNonQuery();

                    MessageBox.Show("Successfully deleted");
                    LoadDataIntoGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error deleting project: " + ex.Message);
            }
        }

        private void ProjectGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    Add.Enabled = false;
                    DeleteBtn.Enabled = true;
                    UpdateBtn.Enabled = true;
                    DataGridViewRow selectedRow = ProjectGrid.SelectedRows[0];
                    title.Text = selectedRow.Cells["Title"].Value.ToString();
                    description.Text = selectedRow.Cells["Description"].Value.ToString();
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void ProjectGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    DataGridViewRow selectedRow = ProjectGrid.Rows[e.RowIndex];
                    Add.Enabled = false;
                    DeleteBtn.Enabled = true;
                    UpdateBtn.Enabled = true;
                    title.Text = selectedRow.Cells["Title"].Value.ToString();
                    description.Text = selectedRow.Cells["Description"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occurs: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Add.Enabled = true;
            UpdateBtn.Enabled = false;
            DeleteBtn.Enabled = false;
            title.Text = "Write title of the Project here";
            description.Text = "Write description of the project here";
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            // update project in database and refresh the grid
            try
            {
                if (ProjectGrid.SelectedRows.Count > 0)
                {
                    DataGridViewRow selectedRow = ProjectGrid.SelectedRows[0];
                    int projectId = (int)selectedRow.Cells["Id"].Value;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE Project SET Description = @Description, Title = @Title WHERE Id = @Id", con);
                    cmd.Parameters.AddWithValue("@Description", description.Text);
                    cmd.Parameters.AddWithValue("@Title", title.Text);
                    cmd.Parameters.AddWithValue("@Id", projectId);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully updated");
                    LoadDataIntoGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating project: " + ex.Message);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
    }
}
