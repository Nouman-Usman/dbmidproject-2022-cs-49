﻿using MidProject;
using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DbMidProjCS49.UI
{
    public partial class AdvisorMenu : Form
    {
        int id_Text = 0;
        public AdvisorMenu()
        {
            InitializeComponent();
            Delete.Enabled = false;
            update.Enabled = false;
            Add.Enabled = true;
        }

        private void firstName_Enter(object sender, EventArgs e)
        {
            if (firstName.Text == "First Name")
            {
                firstName.Text = "";
                firstName.ForeColor = Color.Black;
            }
        }

        private void firstName_Click(object sender, EventArgs e)
        {
            if (firstName.Text == "First Name")
            {
                firstName.Text = "";
                firstName.ForeColor = Color.Black;
            }
        }

        private void lastName_Enter(object sender, EventArgs e)
        {
            if (lastName.Text == "Last Name")
            {
                lastName.Text = "";
                lastName.ForeColor = Color.Black;
            }
        }

        private void lastName_Click(object sender, EventArgs e)
        {
            if (lastName.Text == "Last Name")
            {
                lastName.Text = "";
                lastName.ForeColor = Color.Black;
            }
        }

        private void Email_EnabledChanged(object sender, EventArgs e)
        {
            if (Email.Text == "Email")
            {
                Email.Text = "";
                Email.ForeColor = Color.Black;
            }
        }

        private void Email_Click(object sender, EventArgs e)
        {
            if (Email.Text == "Email")
            {
                Email.Text = "";
                Email.ForeColor = Color.Black;
            }
        }

        private void Salary_Enter(object sender, EventArgs e)
        {
            if (Salary.Text == "Salary")
            {
                Salary.Text = "";
                Salary.ForeColor = Color.Black;
            }
        }

        private void Salary_Click(object sender, EventArgs e)
        {
            if (Salary.Text == "Salary")
            {
                Salary.Text = "";
                Salary.ForeColor = Color.Black;
            }
        }

        private void Contact_Enter(object sender, EventArgs e)
        {
            if (Contact.Text == "Contact")
            {
                Contact.Text = "";
                Contact.ForeColor = Color.Black;
            }
        }

        private void Contact_Click(object sender, EventArgs e)
        {
            if (Contact.Text == "Contact")
            {
                Contact.Text = "";
                Contact.ForeColor = Color.Black;
            }
        }
        private void LoadDataIntoGrid()
        {
            AdvisorGrid.ReadOnly = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Ad.Id , Ad.Salary, Ad.Designation , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Advisor as Ad\r\njoin Person as P\r\n on Ad.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender", con);
            //cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataTable.Columns.Add("Designation.", typeof(string));
            foreach (DataRow row in dataTable.Rows)
            {
                string designationtring = (row["Designation"].ToString() == "1") ? "Professor" : (row["Designation"].ToString() == "2") ? "Associate Professor" : (row["Designation"].ToString() == "3") ? "Assistant Professor" : (row["Designation"].ToString() == "4") ? "Lecturer" : "Unknown";
                row["Designation."] = designationtring;
            }
            dataTable.Columns.Remove("Designation");
            AdvisorGrid.DataSource = dataTable;
        }
        private void Add_Click(object sender, EventArgs e)
        {
            if (DbMidProjCS49.ValidationsAndParsings.isEveryFieldSet(new TextBox[] { firstName, lastName, Contact, Email, Salary }) && DbMidProjCS49.ValidationsAndParsings.isNumber(Contact.Text) && DbMidProjCS49.ValidationsAndParsings.IsValidEmail(Email.Text) && DbMidProjCS49.ValidationsAndParsings.isNumber(Salary.Text))
            {
                var con = Configuration.getInstance().getConnection();
                try
                {
                    SqlCommand checkCmd = new SqlCommand("SELECT COUNT(*) FROM Advisor WHERE Id IN (SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email AND DateOfBirth = @DOB AND Gender = @Gender)", con);
                    checkCmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                    checkCmd.Parameters.AddWithValue("@LastName", lastName.Text);
                    checkCmd.Parameters.AddWithValue("@Contact", Contact.Text);
                    checkCmd.Parameters.AddWithValue("@Email", Email.Text);
                    date.Format = DateTimePickerFormat.Custom;
                    date.CustomFormat = "yyyy-MM-dd";
                    checkCmd.Parameters.AddWithValue("@DOB", date.Text);
                    checkCmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);

                    int count = (int)checkCmd.ExecuteScalar();
                    if (count > 0)
                    {
                        MessageBox.Show("An advisor with the same name already exists in the database.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        // Insert into Person table
                        SqlCommand cmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) VALUES (@FirstName, @LastName, @Contact, @Email, @DOB, @Gender)", con);
                        cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                        cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                        cmd.Parameters.AddWithValue("@Contact", Contact.Text);
                        cmd.Parameters.AddWithValue("@Email", Email.Text);
                        cmd.Parameters.AddWithValue("@DOB", date.Text);
                        cmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                        cmd.ExecuteNonQuery();

                        // Insert into Advisor table
                        SqlCommand cmd1 = new SqlCommand("INSERT INTO Advisor (Id, Designation, Salary) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email AND DateOfBirth = @DOB AND Gender = @Gender), @Designation, @Salary)", con);
                        cmd1.Parameters.AddWithValue("@FirstName", firstName.Text);
                        cmd1.Parameters.AddWithValue("@LastName", lastName.Text);
                        cmd1.Parameters.AddWithValue("@Contact", Contact.Text);
                        cmd1.Parameters.AddWithValue("@Email", Email.Text);
                        cmd1.Parameters.AddWithValue("@DOB", date.Text);
                        cmd1.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                        cmd1.Parameters.AddWithValue("@Designation", designation.SelectedIndex);
                        cmd1.Parameters.AddWithValue("@Salary", int.Parse(Salary.Text));
                        cmd1.ExecuteNonQuery();
                        MessageBox.Show("Advisor added successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDataIntoGrid();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Valid Data");
            }
        }

        private void AdvisorMenu_Load(object sender, EventArgs e)
        {
            LoadDataIntoGrid();
            date.MaxDate = DateTime.Now;
        }

        private void update_Click(object sender, EventArgs e)
        {
            Add.Enabled = false;
            update.Enabled = true;
            Delete.Enabled = true;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("update Person set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID \r\n  update Advisor set Salary = @Salary, Designation = @Designation where Id = @ID", con);
                cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                cmd.Parameters.AddWithValue("@Contact", Contact.Text);
                cmd.Parameters.AddWithValue("@Email", Email.Text);
                cmd.Parameters.AddWithValue("@ID", id_Text);
                cmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                cmd.Parameters.AddWithValue("@Salary", int.Parse(Salary.Text));
                cmd.Parameters.AddWithValue("@Designation", designation.SelectedIndex);
                date.Format = DateTimePickerFormat.Custom;
                date.CustomFormat = "yyyy-MM-dd";
                cmd.Parameters.AddWithValue("@DOB", date.Text);
                cmd.ExecuteNonQuery();
                LoadDataIntoGrid();
                MessageBox.Show("Successfully updated");
                AdminOn();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Add.Enabled = true;
            update.Enabled = false;
            Delete.Enabled = false;
            firstName.Text = "First Name";
            lastName.Text = "Last Name";
            Contact.Text = "Contact";
            Email.Text = "Email";
            Salary.Text = "Salary";
        }

        private void firstName_Leave(object sender, EventArgs e)
        {
            if (firstName.Text == "")
            {
                firstName.Text = "First Name";
                firstName.ForeColor = Color.Black;
            }
        }

        private void lastName_Leave(object sender, EventArgs e)
        {
            if (lastName.Text == "")
            {
                lastName.Text = "Last Name";
                lastName.ForeColor = Color.Black;
            }
        }

        private void Email_Leave(object sender, EventArgs e)
        {
            if (Email.Text == "")
            {
                Email.Text = "Email";
                Email.ForeColor = Color.Black;
            }
        }

        private void Contact_Leave(object sender, EventArgs e)
        {
            if (Contact.Text == "")
            {
                Contact.Text = "Contact";
                Contact.ForeColor = Color.Black;
            }
        }

        private void AdvisorGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Add.Enabled = false;
            update.Enabled = true;
            Delete.Enabled = true;
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    DataGridViewRow selectedRow = AdvisorGrid.Rows[e.RowIndex];
                    firstName.Text = selectedRow.Cells["FirstName"].Value.ToString();
                    lastName.Text = selectedRow.Cells["LastName"].Value.ToString();
                    Contact.Text = selectedRow.Cells["Contact"].Value.ToString();
                    Email.Text = selectedRow.Cells["Email"].Value.ToString();
                    id_Text = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    designation.Text = selectedRow.Cells["Designation."].Value.ToString();
                    Salary.Text = selectedRow.Cells["Salary"].Value.ToString();
                    Gender.Text = selectedRow.Cells["Gender"].Value.ToString();
                    date.Value = Convert.ToDateTime(selectedRow.Cells["DateOfBirth"].Value);
                    Add.Enabled = false;
                    Delete.Enabled = true;
                    update.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occurs: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Text = "Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult == DialogResult.No)
            {
                return;
            }
            else
            {
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[ProjectAdvisor] where AdvisorId = @AdvisorId", con1);
                cmd1.Parameters.AddWithValue("@AdvisorId", id_Text);
                cmd1.ExecuteNonQuery();
                var con4 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Delete from Advisor where Id = @Id", con4);
                cmd2.Parameters.AddWithValue("@Id", id_Text);
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Successfully deleted");
                LoadDataIntoGrid();
                AdminOn();
            }

        }
        private void AdminOn()
        {
            Add.Enabled = true;
            update.Enabled = false;
            Delete.Enabled = false;
            firstName.Text = "First Name";
            lastName.Text = "Last Name";
            Contact.Text = "Contact";
            Email.Text = "Email";
            Salary.Text = "Salary";

        }

        private void AdvisorGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Add.Enabled = false;
            Delete.Enabled = true;
            update.Enabled = true;
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    DataGridViewRow selectedRow = AdvisorGrid.Rows[e.RowIndex];
                    firstName.Text = selectedRow.Cells["FirstName"].Value.ToString();
                    lastName.Text = selectedRow.Cells["LastName"].Value.ToString();
                    Contact.Text = selectedRow.Cells["Contact"].Value.ToString();
                    Email.Text = selectedRow.Cells["Email"].Value.ToString();
                    id_Text = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    designation.Text = selectedRow.Cells["Designation."].Value.ToString();
                    Salary.Text = selectedRow.Cells["Salary"].Value.ToString();
                    Gender.Text = selectedRow.Cells["Gender"].Value.ToString();
                    date.Value = Convert.ToDateTime(selectedRow.Cells["DateOfBirth"].Value);
                    Add.Enabled = false;
                    Delete.Enabled = true;
                    update.Enabled = true;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
    }
}
