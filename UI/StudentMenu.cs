﻿using MidProject.BL;
using MidProject.DAL;
using DbMidProjCS49;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MidProject;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using Org.BouncyCastle.Crypto;

namespace DbMidProjCS49.UI
{
    public partial class StudentMenu : Form
    {
        public object ValidationsAndParsings { get; private set; }

        int id_Text;
        public StudentMenu()
        {
            InitializeComponent();

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            int gender = -1;
            if (DbMidProjCS49.ValidationsAndParsings.isEveryFieldSet(new TextBox[] { firstName, lastName, Contact, Email, RegNo }) && DbMidProjCS49.ValidationsAndParsings.isNumber(Contact.Text) && DbMidProjCS49.ValidationsAndParsings.IsValidEmail(Email.Text))
            {
                // Check if the registration number already exists
                if (IsRegistrationNumberUnique(RegNo.Text))
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("INSERT INTO Person(FirstName,LastName,Contact,Email,DateOfBirth,Gender) VALUES (@FirstName,@LastName, @Contact,@Email,@DOB, @Gender)", con);
                        SqlCommand cmd1 = new SqlCommand("INSERT INTO Student(Id,RegistrationNo) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DOB AND Gender=@Gender) ,@RegistrationNo)", con);
                        cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                        cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                        cmd.Parameters.AddWithValue("@Contact", Contact.Text);
                        cmd.Parameters.AddWithValue("@Email", Email.Text);
                        cmd1.Parameters.AddWithValue("@FirstName", firstName.Text);
                        cmd1.Parameters.AddWithValue("@LastName", lastName.Text);
                        cmd1.Parameters.AddWithValue("@Contact", Contact.Text);
                        cmd1.Parameters.AddWithValue("@Email", Email.Text);
                        cmd1.Parameters.AddWithValue("@RegistrationNo", RegNo.Text);
                        if (Gender.SelectedItem == "Male")
                        {
                            gender = 1;
                        }
                        if (Gender.SelectedItem == "Female")
                        {
                            gender = 2;
                        }
                        date.Format = DateTimePickerFormat.Custom;
                        date.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@Gender", gender);
                        cmd.Parameters.AddWithValue("@DOB", date.Text);
                        cmd1.Parameters.AddWithValue("@Gender", gender);
                        cmd1.Parameters.AddWithValue("@DOB", date.Text);
                        cmd.ExecuteNonQuery();
                        cmd1.ExecuteNonQuery();
                        MessageBox.Show("Successfully saved");
                        LoadDataIntoGrid();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message);
                    }
                }
                else
                {
                    MessageBox.Show("A student with this registration number already exists.");
                }
            }
        }
        private bool IsRegistrationNumberUnique(string regNo)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM Student WHERE RegistrationNo = @RegistrationNo", con);
                cmd.Parameters.AddWithValue("@RegistrationNo", regNo);
                int count = (int)cmd.ExecuteScalar();
                return count == 0; // If count is 0, the registration number is unique
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error checking registration number uniqueness: " + ex.Message);
                return false;
            }
        }
        private void LoadDataIntoGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Student.Id, RegistrationNo, FirstName, LastName, Contact, Email, DateOfBirth, Gender from Person join Student on Person.Id = Student.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dt.Columns.Add("Gender.", typeof(string));
            foreach (DataRow row in dt.Rows)
            {
                int genderValue = Convert.ToInt32(row["Gender"]);
                string genderString = (genderValue == 1) ? "Male" : (genderValue == 2) ? "Female" : "Unknown";
                row["Gender."] = genderString;
            }
            dt.Columns.Remove("Gender");
            StudentGrid.DataSource = dt;
            cmd.ExecuteNonQuery();
        }

        private void StudentMenu_Load(object sender, EventArgs e)
        {
            LoadDataIntoGrid();
            date.MaxDate = DateTime.Now;
            update.Enabled = false;
            Delete.Enabled = false;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("update Person set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID \r\n update Student set RegistrationNo = @RegistrationNumber where Id = @ID ", con);
                cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                cmd.Parameters.AddWithValue("@Contact", Contact.Text);
                cmd.Parameters.AddWithValue("@Email", Email.Text);
                cmd.Parameters.AddWithValue("@ID", id_Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", RegNo.Text);
                cmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                date.Format = DateTimePickerFormat.Custom;
                date.CustomFormat = "yyyy-MM-dd";
                cmd.Parameters.AddWithValue("@DOB", date.Text);
                cmd.ExecuteNonQuery();
                LoadDataIntoGrid();
                MessageBox.Show("Successfully updated");
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void Revert(object sender, EventArgs e)
        {

            Add.Enabled = true;
            update.Enabled = false;
            Delete.Enabled = false;
            firstName.Text = "First Name";
            lastName.Text = "Last Name";
            Contact.Text = "Contact";
            Email.Text = "Email";
            RegNo.Text = "Registration" + " " + "No";

        }

        private void firstName_Enter(object sender, EventArgs e)
        {
            if (firstName.Text == "First Name")
            {
                firstName.Text = "";
                firstName.ForeColor = Color.Black;
            }
        }

        private void lastName_Enter(object sender, EventArgs e)
        {
            if (lastName.Text == "Last Name")
            {
                lastName.Text = "";
                lastName.ForeColor = Color.Black;
            }
        }

        private void Contact_Enter(object sender, EventArgs e)
        {
            if (Contact.Text == "Contact")
            {
                Contact.Text = "";
                Contact.ForeColor = Color.Black;
            }
        }

        private void Email_Enter(object sender, EventArgs e)
        {
            if (Email.Text == "Email")
            {
                Email.Text = "";
                Email.ForeColor = Color.Black;
            }
        }

        private void RegNo_Enter(object sender, EventArgs e)
        {
            if (RegNo.Text == "Registration No")
            {
                RegNo.Text = "";
                RegNo.ForeColor = Color.Black;
            }
        }

        private void firstName_Click(object sender, EventArgs e)
        {
            if (firstName.Text == "First Name")
            {
                firstName.Text = "";
                firstName.ForeColor = Color.Black;
            }
        }

        private void lastName_Click(object sender, EventArgs e)
        {
            if (lastName.Text == "Last Name")
            {
                lastName.Text = "";
                lastName.ForeColor = Color.Black;
            }
        }

        private void Email_Click(object sender, EventArgs e)
        {
            if (Email.Text == "Email")
            {
                Email.Text = "";
                Email.ForeColor = Color.Black;
            }
        }

        private void Contact_Click(object sender, EventArgs e)
        {
            if (Contact.Text == "Contact")
            {
                Contact.Text = "";
                Contact.ForeColor = Color.Black;
            }
        }

        private void RegNo_Click(object sender, EventArgs e)
        {
            if (RegNo.Text == "Registration No")
            {
                RegNo.Text = "";
                RegNo.ForeColor = Color.Black;
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Add.Enabled = false;
            update.Enabled = true;
            Delete.Enabled = true;
            try
            {
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[GroupStudent] where StudentId = @StudentId", con1);
                cmd1.Parameters.AddWithValue("@StudentId", id_Text);
                cmd1.ExecuteNonQuery();

                var con4 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Delete from Student where Id = @Id", con4);
                cmd2.Parameters.AddWithValue("@Id", id_Text);
                cmd2.ExecuteNonQuery();
                LoadDataIntoGrid();
                MessageBox.Show("Successfully deleted");
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void StudentGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    DataGridViewRow selectedRow = StudentGrid.Rows[e.RowIndex];

                    // Update the textboxes with the selected row data
                    firstName.Text = selectedRow.Cells["FirstName"].Value.ToString();
                    lastName.Text = selectedRow.Cells["LastName"].Value.ToString();
                    Contact.Text = selectedRow.Cells["Contact"].Value.ToString();
                    Email.Text = selectedRow.Cells["Email"].Value.ToString();
                    id_Text = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    RegNo.Text = selectedRow.Cells["RegistrationNo"].Value.ToString();
                    Gender.Text = selectedRow.Cells["Gender."].Value.ToString();
                    date.Value = Convert.ToDateTime(selectedRow.Cells["DateOfBirth"].Value);

                    // Enable/disable buttons as needed
                    Add.Enabled = false;
                    Delete.Enabled = true;
                    update.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occurs: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void StudentGrid_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0) { return; }
                else
                {
                    Add.Enabled = false;
                    Delete.Enabled = true;
                    update.Enabled = true;
                    DataGridViewRow selectedRow = StudentGrid.SelectedRows[0];
                    firstName.Text = selectedRow.Cells["FirstName"].Value.ToString();
                    lastName.Text = selectedRow.Cells["LastName"].Value.ToString();
                    Contact.Text = selectedRow.Cells["Contact"].Value.ToString();
                    Email.Text = selectedRow.Cells["Email"].Value.ToString();
                    id_Text = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                    Gender.Text = selectedRow.Cells["Gender."].Value.ToString();
                    RegNo.Text = selectedRow.Cells["RegistrationNo"].Value.ToString();
                    date.Value = Convert.ToDateTime(selectedRow.Cells["DateOfBirth"].Value);
                }
            }
            catch (Exception)
            {
                  return;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
    }
}
