﻿using DbMidProjCS49.UI;
using midtermProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DbMidProjCS49.UI
{
    public partial class MainForm : Form
    {
        private int count = 0;
        public MainForm()
        {
            InitializeComponent();
            HideAll();
        }

        private void MenuClick(object sender, EventArgs e)
        {
            if (count == 0)
            {
                Body.Width = 100;
                SideMenu.Width = 1000;
                Buttons.Show();
                count = 1; 
            }
            else
            {
                HideAll();
                SideMenu.Width = 100;
                Body.Width = 1000;
                count = 0;
            }
        }

       
        private void ExitClick(object sender, EventArgs e)
        {
            MessageBox.Show(Text = "Are you sure you want to exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DialogResult == DialogResult.Yes)
            {
                System.Windows.Forms.Application.Exit();
            }
        }
        private void HideAll()
        {
            Buttons.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            StudentMenu studentMenu = new StudentMenu();
            studentMenu.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();    
            AdvisorMenu advisorMenu = new AdvisorMenu();
            advisorMenu.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProjectMenu projectMenu = new ProjectMenu();
            projectMenu.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            GroupsMenu groupsMenu = new GroupsMenu();
            groupsMenu.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            EvaluationMenu evaluationMenu = new EvaluationMenu();
            evaluationMenu.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            GroupEvaluation groupEvaluation = new GroupEvaluation();
            groupEvaluation.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            GenerateReport generateReport = new GenerateReport();
            generateReport.Show();
        }
    }
}
