# CS 262L - Database Systems Mid Project 1

## Introduction

This project is a part of the mid-term lab exam for CS 262L - Database Systems. It involves the development of a desktop application using Windows Form Application in C#. The application is intended to streamline the management process of final year projects at the Department of Computer Science, UET Lahore.

## Instructor

- **Mr. Nazeef Ul Haq (Lab)**

## Instructions

- The project is to be completed individually as part of the mid-term lab exam.
- Strict measures against plagiarism will be enforced.
- Utilize the provided database named ProjectA for connectivity with the frontend.
- Manage the project on GitLab from day one, committing changes regularly.
- Entity Framework is prohibited; use queries for data retrieval and manipulation.
- Submission will be through Eduko, excluding binary files.
- Any alteration to the database schema is prohibited.

## Grading Criteria

The project will be evaluated based on the following parameters:

- Completion of project features
- Professional and user-friendly UI/UX
- Database connectivity
- Effective exception handling and error messages
- Responsive UI
- Maintainable, readable, and modular code
- Professional-style PDF reports
- Flexibility to accommodate additional features and reports

## Case Study

The Department of Computer Science, UET Lahore, manages final year projects through a committee. The process involves project title selection, group formation, advisor assignment, evaluations, etc. Currently managed through spreadsheets, the aim is to develop a desktop application for streamlined management.

### Features to Implement:

- Manage Students
- Manage Advisors
- Manage Projects
- Formation and management of Student Groups
- Assignment of projects to student groups
- Assignment of multiple advisors to projects
- Evaluation management
- Marking evaluations against student groups

## Database Design

Database Design Diagram for mid-project 1 is provided for reference.

### [Database Design Diagram](https://drive.google.com/file/d/1EPyl2Ez0zwi4lz8b6aDiRw80geR62RR2/view?usp=sharing)

## Database Scripts

Database scripts are available at [http://bit.ly/ProjectADb](http://bit.ly/ProjectADb).

## Readme

### Getting Started

1. Clone the project repository from GitLab.
2. Restore the provided database (ProjectA) for connectivity with the application.
3. Open the solution in Visual Studio.
4. Build and run the project.

### Usage

1. Upon launching the application, navigate through the various modules using the sidebar.
2. Manage students, advisors, and projects as required.
3. Form student groups and assign projects.
4. Manage evaluations and mark them against student groups.
5. Generate reports in PDF format as needed.

##Don't Know How to Run The Code?
Watch This Video [https://www.youtube.com/watch?v=J1ysuX4lIhc](https://www.youtube.com/watch?v=J1ysuX4lIhc).

### Contributing

- Fork the repository on GitLab.
- Implement desired features or fixes.
- Create a merge request for review.
