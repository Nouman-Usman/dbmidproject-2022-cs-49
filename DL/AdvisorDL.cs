﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    class AdvisorDL : PersonDL
    {
        List<Advisor> lst;
        List<string> IDs;
        public DataTable getDesginationValues()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select value from Lookup where category = \'DESIGNATION\'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException ex)
            {
                return null;
            }


        }
        public int getDesignationID(string desination)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id  from Lookup where Value = @designation", con);
                cmd.Parameters.AddWithValue("@designation", desination);
                int designationId = int.Parse(cmd.ExecuteScalar().ToString());
                return designationId;
            }
            catch (SqlException ex)
            {
                return -1;
            }

        }
        public bool insert(Advisor advisor)
        {
            string Id = base.insert(advisor.FirstName, advisor.LastName, advisor.Contact,
                            advisor.Email, advisor.DateOfBirth, advisor.Gender);
            if (Id == null)
                return false;
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = Id;
            columns[1] = getDesignationID(advisor.Designation).ToString();
            columns[2] = advisor.Salary;
            return base.insert("advisor", columns);

        }
        public bool update(Advisor advisor)
        {
            if (!base.update(advisor.Id, advisor.FirstName, advisor.LastName, advisor.Contact,
                            advisor.Email, advisor.DateOfBirth, advisor.Gender))
                return false;
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["Designation"] = getDesignationID(advisor.Designation).ToString();
            columns["Salary"] = advisor.Salary;
            if (!base.update("advisor", columns, "id", advisor.Id))
                return false;
            return true;
        }
        public bool fetchRecords()
        {
            lst = new List<Advisor>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["advisor.Id"] = "ID";
            columns["person.FirstName"] = "[First Name]";
            columns["person.LastName"] = "[Last Name]";
            columns["person.Contact"] = "[Contact]";
            columns["person.Email"] = "[Email]";
            columns["(select value from Lookup where id = advisor.Designation)"] = "[Designation]";
            columns["advisor.Salary"] = "[Salary]";
            columns["CONVERT(VARCHAR(10), person.DateOfBirth , 101)"] = "[Date Of Birth]";
            columns["(select value from Lookup where id = person.Gender)"] = "Gender";


            DataTable dt = base.retrieveWithjoin("advisor", "person", columns, "advisor.Id", "person.Id");

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }
        public bool fetchAdvisorsForProject()
        {

            SqlCommand cmd = new SqlCommand("select CONCAT(a.Id, '-' ,FirstName , ' ' , LastName) as name from advisor a " +
                "join person p on p.id = a.id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt == null)
                return false;
            fillListForAdvisor(dt);
            return true;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string firstName = dt.Rows[i].ItemArray[1].ToString();
                string lastName = dt.Rows[i].ItemArray[2].ToString();
                string contact = dt.Rows[i].ItemArray[3].ToString();
                string email = dt.Rows[i].ItemArray[4].ToString();
                string designation = dt.Rows[i].ItemArray[5].ToString();
                string salary = dt.Rows[i].ItemArray[6].ToString();
                string dob = dt.Rows[i].ItemArray[7].ToString();
                string gender = dt.Rows[i].ItemArray[8].ToString();
                Advisor advisor = new Advisor(id, firstName, lastName, contact, email, dob,
                                                gender, designation, salary);
                lst.Add(advisor);

            }

        }
        private void fillListForAdvisor(DataTable dt)
        {
            IDs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IDs.Add(dt.Rows[i][0].ToString());
            }
        }
        public List<Advisor> getList()
        {
            return lst;
        }
        public List<string> getAdvisorListForProject()
        {

            return IDs;
        }
    }
}
