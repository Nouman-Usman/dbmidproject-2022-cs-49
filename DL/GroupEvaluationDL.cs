﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    class GroupEvaluationDL : CRUD
    {
        //List<GroupEvaluation> lst;
        List<Evaluation> Lst;

        public string getObtainedMarks(string groupId, string evaluationId)
        {

            string query = "select ObtainedMarks from GroupEvaluation where GroupId = @groupId AND EvaluationId = @evaluationId";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@groupId", groupId);
            cmd.Parameters.AddWithValue("@evaluationId", evaluationId);
            string om = cmd.ExecuteScalar().ToString();
            return om;
        }

        public bool updateObatinedMarks(string groupId, string evaluationId, string marks)
        {
            try
            {
                string query = "Update GroupEvaluation set ObtainedMarks = @marks where GroupId = @groupId AND EvaluationId = @evaluationId";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@marks", marks);
                cmd.Parameters.AddWithValue("@evaluationId", evaluationId);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }
        public bool insert(GroupEvaluation groupEvaluation)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = groupEvaluation.GroupId;
            columns[1] = groupEvaluation.EvaluationId;
            columns[2] = groupEvaluation.ObtainedMarks;
            columns[3] = groupEvaluation.EvaluationDate;
            return base.insert("GroupEvaluation", columns);
        }
        public bool fetchPendingEvaluations(string groupId)
        {
            Lst = new List<Evaluation>();
            string query = "select * " +
                            "from Evaluation  " +
                            "where id not in (select EvaluationId " +
                            "					from GroupEvaluation " +
                            "					where GroupId = " + groupId + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt == null)
                return false;
            fillList(dt);
            return true;

        }

        public bool fetchGroupEvaluations(string groupId)
        {
            Lst = new List<Evaluation>();
            string query = "select * " +
                            "from Evaluation  " +
                            "where id in (select EvaluationId " +
                            "					from GroupEvaluation " +
                            "					where GroupId = " + groupId + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt == null)
                return false;
            fillList(dt);
            return true;

        }
        public DataTable fetchGroupEvaluationsDataTable(string groupId)
        {
            Lst = new List<Evaluation>();
            string query = "select * " +
                            "from Evaluation  " +
                            "where id in (select EvaluationId " +
                            "					from GroupEvaluation " +
                            "					where GroupId = " + groupId + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string name = dt.Rows[i].ItemArray[1].ToString();
                string totalMarks = dt.Rows[i].ItemArray[2].ToString();
                string weightage = dt.Rows[i].ItemArray[3].ToString();
                Evaluation evaluation = new Evaluation(id, name, totalMarks, weightage);
                Lst.Add(evaluation);

            }

        }
        public List<Evaluation> getList()
        {
            return Lst;
        }
    }
}
