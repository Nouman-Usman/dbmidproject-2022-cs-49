﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    public class ProjectAdvisorDL : CRUD
    {
        private string getAdvisorRole(string role)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select id from Lookup where  value = @role", con);
                cmd.Parameters.AddWithValue("@role", role);
                string id = cmd.ExecuteScalar().ToString();
                return id;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool insert(ProjectAdvisor projectAdvisor)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = projectAdvisor.AdvisorId;
            columns[1] = projectAdvisor.ProjectId;
            columns[2] = getAdvisorRole(projectAdvisor.AdvisorRole);
            columns[3] = projectAdvisor.AssignmentDate;
            return base.insert("ProjectAdvisor", columns);
        }
    }
}
