﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    public class GroupDL : CRUD
    {
        List<Group> lst;
        /// <summary>
        /// Add Group and Return Its Id
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public string insert(Group group)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = group.Created_on;
            string Id = base.insertAndGet("[Group]", columns, "id");
            if (Id == null)
                return null;
            return Id;
        }

        public bool fetchRecords()
        {
            lst = new List<Group>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["Id"] = "ID";
            columns["CONVERT(VARCHAR(10), Created_on, 101)"] = "Created_On";
            DataTable dt;
            dt = base.retrieve("[group]", columns);
            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string created_on = dt.Rows[i].ItemArray[1].ToString();
                Group group = new Group(id, created_on);
                lst.Add(group);
            }
        }

        public List<Group> getList()
        {
            return lst;
        }

        public bool fetchNonGroupStudents()
        {
            lst = new List<Group>();
            string query = "select Id  , CONVERT(VARCHAR(10), g.Created_on , 101) AS Created_On " +
                            "from [group] g " +
                            "where g.Id not in(select id " +
                            "from [group] g " +
                            "join groupProject gp " +
                            "on g.Id = gp.GroupId)";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt == null)
                return false;
            fillList(dt);
            return true;

        }
    }
}

