﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    public class ProjectDL : CRUD
    {
        List<Project> lst;
        public bool insert(Project project)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = project.Description;
            columns[1] = project.Title;
            return base.insert("project", columns);
        }

        public bool update(Project project)
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns["Description"] = project.Description;
            columns["Title"] = project.Title;
            return base.update("project", columns, "id", project.Id);
        }


        public bool fetchRecords()
        {
            lst = new List<Project>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["Id"] = "ID";
            columns["Title"] = "[Title]";
            columns["Description"] = "[Description]";

            DataTable dt = base.retrieve("project", columns);

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }

        public bool fetchNonGroupProjects()
        {
            lst = new List<Project>();
            string query = "select Id as ID , Title , Description " +
                            "from Project " +
                            "where Id not in ( " +
                            "					select p.Id " +
                            "					from Project p " +
                            "					join GroupProject gp " +
                            "					on p.Id = gp.ProjectId) ";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt == null)
                return false;
            fillList(dt);
            return true;

        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i].ItemArray[0].ToString();
                string title = dt.Rows[i].ItemArray[1].ToString();
                string description = dt.Rows[i].ItemArray[2].ToString();
                Project project = new Project(id, description, title);
                lst.Add(project);

            }

        }
        public List<Project> getList()
        {
            return lst;
        }

        public bool fetchById(string id)
        {
            lst = new List<Project>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["Id"] = "ID";
            columns["Title"] = "[Title]";
            columns["Description"] = "[Description]";
            DataTable dt = base.retrieve("Project", columns, "Id", id);

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }

    }
}
