﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.BL;
namespace MidProject.DAL
{
    public class GroupProjectDL : CRUD
    {
        List<GroupProject> lst;
        public bool insert(GroupProject groupProject)
        {
            Dictionary<int, string> columns = new Dictionary<int, string>();
            columns[0] = groupProject.ProjectId;
            columns[1] = groupProject.GroupId;
            columns[2] = groupProject.AssignmentDate;
            return base.insert("groupProject", columns);
        }
        public bool fetchRecords()
        {
            lst = new List<GroupProject>();
            Dictionary<string, string> columns = new Dictionary<string, string>();

            columns["ProjectId"] = "[Project ID]";
            columns["GroupId"] = "[Group Id]";
            columns["CONVERT(VARCHAR(10), AssignmentDate, 101)"] = "[Assignment Date]";

            DataTable dt = base.retrieve("GroupProject", columns);

            if (dt == null)
                return false;
            fillList(dt);
            return true;
        }
        private void fillList(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string projectId = dt.Rows[i].ItemArray[0].ToString();
                string groupId = dt.Rows[i].ItemArray[1].ToString();
                string AssignmentDate = dt.Rows[i].ItemArray[2].ToString();
                GroupProject groupProject = new GroupProject(projectId, groupId, AssignmentDate);
                lst.Add(groupProject);

            }

        }
        public List<GroupProject> getList()
        {
            return lst;
        }
    }
}
