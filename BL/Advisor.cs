﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Advisor : Person
    {
        string designation;
        string salary;
        /// <summary>
        /// This create Advisor from App layer
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="contact"></param>
        /// <param name="email"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="gender"></param>
        /// <param name="registrationNumber"></param>
        public Advisor(string firstName, string lastName, string contact, string email,
                        string dateOfBirth, string gender, string designation, string salary) : base(firstName, lastName, contact, email, dateOfBirth, gender)
        {
            this.Salary = salary;
            this.Designation = designation;
        }
        /// <summary>
        /// this fucntion create advisor from Database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="contact"></param>
        /// <param name="email"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="gender"></param>
        /// <param name="designation"></param>
        /// <param name="salary"></param>
        public Advisor(string id, string firstName, string lastName, string contact, string email,
                       string dateOfBirth, string gender, string designation, string salary) : base(id, firstName, lastName, contact, email, dateOfBirth, gender)
        {
            this.Salary = salary;
            this.Designation = designation;
        }

        public string Designation { get => designation; set => designation = value; }
        public string Salary { get => salary; set => salary = value; }
    }
}
