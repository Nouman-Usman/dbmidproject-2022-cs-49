﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MidProject.DAL;
namespace MidProject.BL
{
    public class GroupEvaluation
    {
        string groupId;
        string evaluationId;
        string obtainedMarks;
        string evaluationDate;

        public GroupEvaluation(string groupId, string evaluationId, string obtainedMarks, string evaluationDate)
        {
            this.GroupId = groupId;
            this.EvaluationId = evaluationId;
            this.ObtainedMarks = obtainedMarks;
            this.EvaluationDate = evaluationDate;
        }

        public string GroupId { get => groupId; set => groupId = value; }
        public string EvaluationId { get => evaluationId; set => evaluationId = value; }
        public string ObtainedMarks { get => obtainedMarks; set => obtainedMarks = value; }
        public string EvaluationDate { get => evaluationDate; set => evaluationDate = value; }
    }
}
