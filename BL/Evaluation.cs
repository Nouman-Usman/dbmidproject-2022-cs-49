﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Evaluation
    {
        string id;
        string name;
        string totalmarks;
        string totalWeightage;

        public Evaluation(string id, string name, string totalmarks, string totalWeightage)
        {
            this.id = id;
            this.name = name;
            this.totalmarks = totalmarks;
            this.totalWeightage = totalWeightage;
        }

        public Evaluation(string name, string totalmarks, string totalWeightage)
        {
            this.name = name;
            this.totalmarks = totalmarks;
            this.totalWeightage = totalWeightage;
        }

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Totalmarks { get => totalmarks; set => totalmarks = value; }
        public string TotalWeightage { get => totalWeightage; set => totalWeightage = value; }
    }
}
