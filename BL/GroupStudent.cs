﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    class GroupStudent
    {
        string groupId;
        string studentId;
        string status;
        string assignmentDate;

        public GroupStudent(string studentId, string status, string assignmentDate, string groupId)
        {
            this.groupId = groupId;
            this.studentId = studentId;
            this.Status = status;
            this.assignmentDate = assignmentDate;
        }
        public string StudentId { get => studentId; set => studentId = value; }
        public string AssignmentDate { get => assignmentDate; set => assignmentDate = value; }
        public string GroupId { get => groupId; set => groupId = value; }
        public string Status { get => status; set => status = value; }
    }
}
