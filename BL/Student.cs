﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class Student : Person
    {
        string registrationNumber;

        /// <summary>
        /// this Constructs student from APP layer
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="contact"></param>
        /// <param name="email"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="gender"></param>
        /// <param name="registrationNumber"></param>
        public Student(string firstName, string lastName, string contact, string email,
                        string dateOfBirth, string gender, string registrationNumber) : base(firstName, lastName, contact, email, dateOfBirth, gender)
        {
            this.RegistrationNumber = registrationNumber;
        }

        /// <summary>
        /// This contructs student from Database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="contact"></param>
        /// <param name="email"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="gender"></param>
        /// <param name="registrationNumber"></param>
        public Student(string id, string firstName, string lastName, string contact, string email,
                        string dateOfBirth, string gender, string registrationNumber) : base(id, firstName, lastName, contact, email, dateOfBirth, gender)
        {
            this.RegistrationNumber = registrationNumber;
        }

        public string RegistrationNumber { get => registrationNumber; set => registrationNumber = value; }
    }
}
