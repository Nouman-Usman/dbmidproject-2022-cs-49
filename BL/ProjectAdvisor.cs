﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidProject.BL
{
    public class ProjectAdvisor
    {
        string advisorId;
        string projectId;
        string advisorRole;
        string assignmentDate;

        public ProjectAdvisor(string advisorId, string projectId, string advisorRole, string assignmentDate)
        {
            this.advisorId = advisorId;
            this.projectId = projectId;
            this.advisorRole = advisorRole;
            this.assignmentDate = assignmentDate;
        }

        public string AdvisorId { get => advisorId; set => advisorId = value; }
        public string ProjectId { get => projectId; set => projectId = value; }
        public string AdvisorRole { get => advisorRole; set => advisorRole = value; }
        public string AssignmentDate { get => assignmentDate; set => assignmentDate = value; }
    }
}
